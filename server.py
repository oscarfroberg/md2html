#!/usr/bin/env python
# -*- coding utf-8 -*-

import web
import re
import glob
import os
import time
from md2html import *

urls = ('/', 'index',
        '/(.+)', 'view'
        )

app = web.application(urls, globals())
web.config.debug = False
postdir = "/var/www/md2html"
render = web.template.render("html/", base="base", globals={"context": "session"}, cache=False)
render_plain = web.template.render("html/", globals={"context": "session"}, cache=False)

def getdescription(infile):
    f = open(infile)
    lines = f.readlines()
    print len(lines)
    if len(lines)>2:
        response = lines[2]
    elif len(lines) == 2:
        response = lines[1]
    elif len(lines) == 1:
        response = line[0]
    else:
        response = "Empty post. This should rarely be the case, but oh well."
    response = response[0:-1] # remove \n
    # remove hyperlinks
    hyperlinks = re.findall(r'\[.*?\]\(.*?\)', response)
    if hyperlinks:
        for i in hyperlinks:
            description = re.search('\[(.*)\]\(', i)
            description = description.group(1)
            response = response.replace(i, description)
    return response

def getfiledate(infile):
    filetime = time.ctime(os.path.getmtime(infile))
    filetime = re.sub('\s\d\d:\d\d:\d\d\s', ', ', filetime)
    filetime = filetime.split(" ")
    filetime = " ".join(filetime)
    return filetime

def getpostlist():
    filelist = sortedlist(postdir+"/posts/*.md")
    return filelist

def gettitles():
    filelist = sortedlist(postdir+"/posts/*.md")
    titles = []
    for i in filelist:
        url = i.split("/")[-1]
        url = url.split(".")[0]
        f = open(i)
        title = f.readline()
        title = title.replace("\n", "")
        filedate = getfiledate(i)
        entry = {}
        entry["date"] = str(filedate)
        entry["title"] = title
        entry["url"] = url
        f.close()
        titles.append(entry)
    return titles

def sortedlist(path):
    files = filter(os.path.isfile, glob.glob(path))
    files.sort(key=lambda x: os.path.getmtime(x), reverse=True)
    return files

class index:
    def GET(self):
        posts = []
        for i in getpostlist():
            html = md2html(i, None)
            posts.append(html)
        return render.index(gettitles())

class view:
    def GET(self, url):
        if url == "favicon.ico":
            return
        if url[-1] == "/":
            url = url[0:-1]
        url = url + ".md"
        posturl = postdir+"/posts/"+url
        postcontent = md2html(posturl, None)
        if postcontent is not None:
            postdate = getfiledate(posturl)
            description = getdescription(posturl)
            return render.view(postcontent, postdate, description)
        else:
            return render.error("Post not found....")

if __name__ == "__main__":
    app.run()

application = app.wsgifunc()
