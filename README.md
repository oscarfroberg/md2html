Okay, so now that the converter is somewhat working, I intend to build a blogging system around it. If you just need the markdown to html converter, you can simply download the md2html.py and use it as you like.

Just really basic markdown support is implemented (all I need):

links: [description](url)
code: ``
headers: # ## ###
italics/bold: *italic* **bold**
unnumbered lists: lines starting with "* "
